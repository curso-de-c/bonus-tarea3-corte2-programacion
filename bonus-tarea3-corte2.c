#include <math.h>
#include <stdio.h>
#ifndef MPI
#define MPI 3.14159265358979323846
#endif
// Definimos una variable(como libreria o variable estática) con el valor apróximado 20 décimales a PI //



// Trabajo hecho por: Jorge Andrés Ballen Ramírez - T.D.S
// Fecha de entrega: 24/04/2024
// Asignatura: Lenguajes de Programación

// BONUS PARA EL TERCER CORTE - NO ENTREGA PRIMERA ACTIVIDAD //

// Hacer un programa en C, que permita calcular el área de un CILINDRO. El
// programa debe pedirle al usuario los datos básicos necesarios y hacer el
// cálculo. //

int main() {
  float radio, altura, resultado;
  printf("                Bienvenido a la calculadora del área de un CILINDRO\n\n");
  printf("El cilindro es un cuerpo geométrico limitado por una superficie cilíndrica cerrada y dos planos que la cortan,\nllamados bases.\n\n");
  printf("Para calcular el área de un cilindro se necesita:\n\n-El radio(segmento desde el centro de una circunferencia hasta cualquier borde del mismo)\n\n-La altura (valor desde el punto más bajo, hasta el más alto del cilindro)\n\n");
  printf("Ingresa el radio del cilindro:\n");
  scanf("%f", &radio);
  printf("Ingresa la altura del cilindro:\n");
  scanf("%f", &altura);
  printf("\n--------------------------------------------------");
  resultado = 2*MPI*radio*(altura+radio);
  printf("\n| El área del CILINDRO ingresado es: %.2fu^3 |\n", resultado);
  printf("--------------------------------------------------");
  
  // u^3 es unidades cubicas o al cubo, en representación de cualquier unidad del área en tercera dimensión //

  // %.#f es para que muestre # decimales //
  return 0;
}